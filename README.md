# abp_rename
abp 重命名脚本工具

#### 使用方式
1. 该脚本只针对 core 项目
2. 将脚本复制到项目跟目录,和 ` .git ` 、` aspnet-core `等目录在同一目录下
3. 修改脚本参数 ` $newName `,修改成要使用的名称,一般建议 "公司名.项目名",例如 `Zrd.AbpDemo`
4. 修改脚本参数 ` $newProjectName `,修改成要使用的项目名称,例如`AbpDemo`
5. 运行脚本
